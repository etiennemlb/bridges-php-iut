<?php
require_once PATH_VUE . '/vueClassement.php';

// contrôleur gérant l'affichage du tableau des scores des joueurs
class ControleurClassement
{
    private $vue;
    private $bd;

    function __construct()
    {
        $this->vue = new VueClassement();
        $this->bd = new ConnexionBD();
    }

    function processLesEntrees()
    {
        // on récupère le tableau des scores depuis le modèle de la bdd
        $data = array('infojoueurs' => $this->bd->listeJoueursStats());

        // on transmet ensuite ce tableau à la vue du classement
        $this->vue->printHTML($data);
    }
}