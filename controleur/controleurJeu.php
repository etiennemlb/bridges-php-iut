<?php
require_once(PATH_VUE . '/vueTableJeu.php');
require_once(PATH_MODELE . '/Partie.php');

// contrôleur gérant l'affichage de l'état de la partie
class ControleurJeu
{

    private $vue;
    private $bd;

    function __construct()
    {
        $this->vue = new VueTableJeu();
        $this->bd = new ConnexionBD();

    }

    // méthode récupérant les données du compte et de la partie pour afficher la page de jeu
    function processLesEntrees()
    {
        // on commence par récupérer les statistiques du joueur pour les afficher pendant la partie
        $data = array('stats' => $this->bd->recupererStats($_SESSION['pseudo']));

        $data['erreur'] = (isset($_SESSION['partie']) ? 'Gagnée' : 'En attente');

        // si le joueur n'a pas encore lancé de partie
        if (isset($_SESSION['partie']) && $_SESSION['partie'] != '') {

            $data['erreur'] = ($_SESSION['derniereerreur'] == null || $_SESSION['derniereerreur'] == '' ? 'Pas de problème' : $_SESSION['derniereerreur']);

            $partie = unserialize($_SESSION['partie']);

            // tableau stockant les caractères qui vont permettre l'affichage des ponts et des villes
            $ascii = array();

            // on commence par stocker dans le tableau, les caractères qui correpondent au ponts
            foreach ($partie->ponts() as $pont) {
                if ($pont->estHorizontal()) {
                    $y = $pont->ville1()->getY();
                    $xm = min($pont->ville1()->getX(), $pont->ville2()->getX());
                    $xM = max($pont->ville1()->getX(), $pont->ville2()->getX());
                    foreach (range($xm, $xM) as $x) {
                        $ascii[$y][$x] = ($pont->nombrePonts() == 1) ? '―' : '═';
                    }
                } else {
                    $x = $pont->ville1()->getX();
                    $ym = min($pont->ville1()->getY(), $pont->ville2()->getY());
                    $yM = max($pont->ville1()->getY(), $pont->ville2()->getY());
                    foreach (range($ym, $yM) as $y) {
                        $ascii[$y][$x] = ($pont->nombrePonts() == 1) ? '│' : '║';
                    }
                }

            }

            // on ajoute les villes dans le tableau par un string composé d'un caractère contenant le nombre de ponts max liés et d'un caractère contenant l'id de la ville
            foreach ($partie->villes()->getVilles() as $ville) {
                $x = $ville->getX();
                $y = $ville->getY();
                $ascii[$y][$x] = $ville->getNombrePontsMax() . '' . $ville->getId();
            }

            // création du tableau de données à transmettre à la vue contenant les caractères représentant la partie, la taille de la grille et une ville éventuellement sélectionnée
            $data['ascii'] = $ascii;
            $data['maxy'] = $partie->villes()->maxX;
            $data['maxx'] = $partie->villes()->maxY;
            $data['select'] = ($partie->isVilleSelect()) ? '' . $partie->villeSelect()->getId() : '-1';
        }

        // on envoie les données à la vue
        $this->vue->printHTML($data);
    }


}