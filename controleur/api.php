<?php
require_once(PATH_MODELE . '/Partie.php');

// contrôleur gerant les appels à l'API permettant de jouer
class Api
{

    private $vue;
    private $bd;

    function __construct()
    {
        $this->vue = new VueTableJeu();
        $this->bd = new ConnexionBD();

    }

    // méthode récupérant les paramètres passés à l'API pour effectuer l'action voulue
    function processLesEntrees()
    {
        // Variable contenant le status de la partir (erreur de création de pont, pont créé etc...)
        $_SESSION['derniereerreur'] = '';

        // si le joueur a cliqué sur le bouton de nouvelle partie
        if (isset($_POST['newgame'])) {
            // si le joueur était en train de jouer à une autre partie, on considère cette partie comme perdue
            if (isset($_SESSION['partie']))
                $this->bd->incrementerParties($_SESSION['pseudo'], false);

            // création et sérialisation de la nouvelle partie
            $tmp = new Partie();
            $_SESSION['partie'] = $tmp->serializeMoi();
        }

        /*
        if (!isset($_SESSION['partie'])) {
            $tmp = new Partie();
            $_SESSION['partie'] = $tmp->serializeMoi();
        }*/

        // si le joueur souhaite annuler une action et si une partie est bien lancée
        if (isset($_POST['undo']) && $_SESSION['partie'] != '') {

            $partie = unserialize($_SESSION['partie']);

            // on annule l'action précédente et on remet à jour l'état du jeu
            $partie->annulerAction();
            $partie->mettreAJour();

            $_SESSION['partie'] = $partie->serializeMoi();
            return;
        }

        // si le joueur n'a pas sélectionné une ville (dernière action possible), on quitte la fonction
        if (!isset($_GET['ville'])) {
            return;
        }

        $partie = unserialize($_SESSION['partie']);

        //si une ville est déjà selectionnee
        if ($partie->isVilleSelect()) {

            //on recupere la ville que le joueur nous envoie et la ville qu'on avait déja
            $villeA = $partie->villes()->getVilleAvecID($_GET['ville']);
            $villeB = $partie->villeSelect();

            //si les villes sont différentes
            if ($villeA != $villeB) {

                // il existe un pont entre ces deux villes ?
                $pontPresent = $partie->rechercherPont($villeA, $villeB);

                //si non
                if ($pontPresent == null) {

                    //si le pont respecte les contraintes liées aux règles du jeu
                    if ($partie->pontPossible($villeA, $villeB)) {
                        $partie->ajouterAction(TypeAction::CreerPont, $villeA, $villeB);
                        //on créé un pont valide donc on désélectionne les villes
                        $partie->deselectVille();

                    } else {
                        // si le pont ne respecte pas les contraintes, on envoie un message d'erreur 
                        $_SESSION['derniereerreur'] = '<span style="color:red">Pont invalide !</span>';
                        $partie->deselectVille();
                    }

                // si il y a déjà un pont entre les deux villes
                } else {

                    //peut-on encore ajouter un pont (2 max) ?

                    //non donc on supprime le double pont entre les deux villes
                    if ($pontPresent->maxPontsAtteint()) {
                        $partie->ajouterAction(TypeAction::SupprimerPont, $villeA, $villeB);
                        $partie->deselectVille();
                    //sinon, on tente d'ajouter le pont si c'est possible
                    } else {
                        if ($partie->pontPossible($villeA, $villeB)) {
                            $partie->ajouterAction(TypeAction::CreerPont, $villeA, $villeB);
                            //on créé un pont valide donc on désélectionne les villes
                            $partie->deselectVille();

                        } else {
                            // si le pont ne respecte pas les contraintes, on envoie un message d'erreur 
                            $_SESSION['derniereerreur'] = '<span style="color:red">Pont invalide !</span>';
                            $partie->deselectVille();
                        }
                    }
                }        
            // si on sélectionne une ville déjà sélectionnée, on a déselectionne
            } else {
                $partie->deselectVille();
            }
        // si aucune ville n'était déjà sélectionnée, on la sélectionne
        } else {
            $partie->selectionnerVille($partie->villes()->getVilleAvecID($_GET['ville']));
        }

        // on met à jour l'état du jeu avec le nouveau pont éventuellement créé
        $partie->mettreAJour();

        // si on détecte que l'état de la partie est réunit les conditions de victoire
        if ($partie->partieGagne()) {
            $_SESSION['derniereerreur'] = '<span style="color:green">Partie Gagnée<span>';

            // enregistre une nouvelle partie gagnée pour le joueur
            $this->bd->incrementerParties($_SESSION['pseudo'], true);

            // on efface la partie
            $_SESSION['partie'] = '';

        } else {
            $_SESSION['partie'] = $partie->serializeMoi();
        }
        return;
    }
}