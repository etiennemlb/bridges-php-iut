<?php
require_once PATH_VUE . '/vueConnexion.php';
require_once PATH_MODELE . '/connexionBD.php';

// contrôleur gerant la connexion des joueurs à leur compte
class ControleurAuthentification
{
    private $vue;
    private $bd;

    function __construct()
    {
        $this->vue = new VueConnexion();
        $this->bd = new ConnexionBD();
    }

    // méthode vérifiant les informations données par l'utilisateur et qui le connecte à son compte
    function processLesEntrees()
    {
       
        // les information nécéssaire ne sont pas présente, on envoie la vue de saisie
        if (!isset($_POST['pseudo']) || !isset($_POST['password'])) {
            $this->vue->printHTML(" ");
            return;
        }
   
        // si une des informations n'a pas été entrée
        if ($_POST['pseudo'] == '' or $_POST['password'] == '') {
            $this->vue->printHTML('Entrez toutes les informations !');
            return;
        }
        
        // si le couple mdp/pseudo est valide (présent dans la bdd)
        if ($this->bd->sessionValide($_POST['pseudo'], $_POST['password'])) {
            $_SESSION['pseudo'] = $_POST['pseudo'];
            $_SESSION['logged'] = true;
            //l e routeur va redispatcher
            redirigerVers('connexion');
            return;
        } else {
            // si le couple mdp/pseudo est faux, on envoie un message d'erreur à l'utilisateur
            $this->vue->printHTML('Mauvais identifiants');
            return;
        }

    }

}