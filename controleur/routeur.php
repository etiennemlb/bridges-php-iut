<?php
session_start();

require_once('config/global.php');

require_once(PATH_CONTROLEUR . '/api.php');
require_once(PATH_CONTROLEUR . '/controleurAuthentification.php');
require_once(PATH_CONTROLEUR . '/controleurJeu.php');
require_once(PATH_CONTROLEUR . '/controleurClassement.php');

require_once(PATH_MODELE . '/connexionBD.php');

// classe redirigeant les requêtes de l'utilisateur
class Routeur
{

  // Traite une requête entrante
  public function routerRequete()
  {
    // si pas de section dans l'url longue, on redirige vers la connexion
    if (!isset($_GET['section'])) {
      redirigerVers('connexion');
      return;
    }
 
    //si l'utilisateur n'est pas connecté et pas sur la connexion, on le redirige vers la connexion
    if (!isset($_SESSION['logged']) && $_GET['section'] != 'connexion') {
      redirigerVers('connexion');
      return;
    }

    // si l'utilistaur nest pas connecté et sur une page de connexion, on lui affiche la vue de connexion
    if (!isset($_SESSION['logged'])) {
      $ctrlAuthentification = new ControleurAuthentification();
      $ctrlAuthentification->processLesEntrees();
      return;
    }

    // à partie de là, on est forcément connecté

    // si utilisateur demande à se déconnecter, on supprime sa variable de session et on le redirige vers la connexion
    if ($_GET['section'] == 'logout') {
      $bd = new ConnexionBD();
      // si une partie était en cours, on considère cette partie comme perdue
      if (isset($_SESSION['partie']) && $_SESSION['partie'] != '') {
        $bd->incrementerParties($_SESSION['pseudo'], false);
      }
      session_destroy();
      redirigerVers('connexion');
      return;
    }

    // si la requête concerne la page de jeu, on passe la requête au contrôleur de jeu
    if ($_GET['section'] == 'tablejeu') {
      $ctrlJeu = new ControleurJeu();
      $ctrlJeu->processLesEntrees();
      return;
    }

    // si la requête concerne la page de classement, on passe la requête au contrôleur d'affichage de classement
    if ($_GET['section'] == 'classement') {
      $ctrlClassement = new ControleurClassement();
      $ctrlClassement->processLesEntrees();
      return;
    }

    // si la requête concerne l'api (une commande de jeu), on passe la requête à l'api puis on redirige vers le contrôleur d'affichage de la partie
    if ($_GET['section'] == 'api') {
      $api = new Api();
      $api->processLesEntrees();
      redirigerVers('tablejeu');
      return;
    }

    // sinon, dans tous les cas, on redirige vers la vue d'affichage de la partie
    redirigerVers('tablejeu');
    return;
  }

}


?>