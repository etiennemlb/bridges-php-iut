<?php 

// vue affichant la page de connexion
class VueConnexion
{
  // affiche la page avec un éventuel message
  function printHTML($message)
  {
    header("Content-type: text/html; charset=utf-8");
    ?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="UTF-8">
  <title>Bridge - Connexion</title>
  <meta name="viewport" content="initial-scale=1.0, width=device-width" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
  <link rel="icon" href="style/image/BridgeFavicon.png" />
  <link rel="stylesheet" href="style/css/style.css">


</head>

<body>

  <div class="login_form">
    <section class="login-wrapper">

      <div class="logo">
        <img src="style/image/logoIUT-Blanc.png" alt=""></a>
      </div>

      <form id="login" method="post" action="index.php?section=connexion">

        <label for="username">Pseudo</label>
        <input required name="pseudo" type="text" autocapitalize="off" autocorrect="off" />
        <i class="fa fa-user fa-lg fa-fw" aria-hidden="true"></i>
        <label for="password">Mot de Passe</label>
        <input class="password" required name="password" type="password" />
        <?php if($message != " ")echo $message; ?>
        <button type="submit" name="soumettre" value="envoyer">Connexion</button>
      </form>

      </section>
  </div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

  <script src="style/js/index.js"></script>
</body>

</html>
<?php

}
}
?>