<?php 

// Vue affichant la page sur laquelle le joueur peut jouer au bridge
class VueTableJeu
{
    // affichage de la vue
    // précondition : data contient les caractères permettant l'affichage de la partie ainsi que des informations sur les statistiques du joueur et sur la taille de la grille de jeu
    function printHTML(array $data)
    {
        ?>
<html>
    <head>
        <title>Bridge</title>
        <meta charset="utf-8" />
        <link rel="icon" href="style/image/BridgeFavicon.png" />
        <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Cherry+Swash'>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>
        <style>

body{
    background: url("style/image/Savin-NY-Website-Background-Web.jpg") no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
body .navbar{
  font-family: 'Cherry Swash', cursive;
}

    span.stretch {
    display:inline-block;
    -webkit-transform:scale(1.4,1); /* Safari and Chrome */
    -moz-transform:scale(1.4,1); /* Firefox */
    -ms-transform:scale(1.4,1); /* IE 9 */
    -o-transform:scale(1.4,1); /* Opera */
    transform:scale(1.4,1); /* W3C */
}

    span.stretchv {
    display:inline-block;
    -webkit-transform:scale(1,1.2); /* Safari and Chrome */
    -moz-transform:scale(1,1.2); /* Firefox */
    -ms-transform:scale(1,1.2); /* IE 9 */
    -o-transform:scale(1,1.2); /* Opera */
    transform:scale(1,1.2); /* W3C */
}

    a {
        text-decoration: none;
        color: #000044;
        text-decoration: blink;
    }

    a.selected {
        text-decoration: none;
        color: #0000ff;
    }

    a:hover {
        text-decoration: none;
        color: #00aa00;
    }

    input[type="submit"] {
        font-family: 'Cherry Swash', sans-serif;
        color: #333;
        background-color: #f8f8f8;
        height: 3em;
        border-radius: 5px;
        border: 0.5px solid #e6e6e6;
        vertical-align: middle;
    }

    input[type="submit"]:hover {
        background-color: #e6e6e6;
    }

    .noselect {
        -webkit-touch-callout: none; /* iOS Safari */
        -webkit-user-select: none; /* Safari */
        -khtml-user-select: none; /* Konqueror HTML */
        -moz-user-select: none; /* Firefox */
        -ms-user-select: none; /* Internet Explorer/Edge */
        user-select: none; /* Non-prefixed version, currently
                             supported by Chrome and Opera */
    }
}

    </style>

      <div class="container-fluid">
<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="index.php">Accueil</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php?section=tablejeu">Jouer</a></li>
            <li><a href="index.php?section=classement">Classement</a></li>
            
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php?section=logout">Deconnexion</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>
</div>
<div align='center'>
<br/><br/><br/><br/>
        <div align='center'>
            <form action="index.php?section=api" method="POST">
    
                <input type="submit" name="newgame" value="Nouvelle parties">
                <input type="submit" name="undo" value="Annuler l'action précédente">
            </form>
            Vous avez gagné(e) <?php echo $data['stats']['partiesGagnees']; ?> parties et joué(e) <?php echo $data['stats']['partiesJouees']; ?> parties.
            <br/><br/>
            <div>
                <table oncontextmenu="return false;" class="noselect" cellpadding=0 cellspacing=0 style="font-size: 3em; border: 3px solid black; text-align: center; margin: 0px; border-collapse: collapse; border-spacing: 0">
                <?php 

                if (isset($data['ascii'])) {
                    $ascii = $data['ascii'];

                    // table des caractères utilisés pour représenter les villes
                    $cities = array();
                    $cities[1] = '①';
                    $cities[2] = '②';
                    $cities[3] = '③';
                    $cities[4] = '④';
                    $cities[5] = '⑤';
                    $cities[6] = '⑥';
                    $cities[7] = '⑦';
                    $cities[8] = '⑧';
                    $cities[9] = '⑨';
                    $cities[10] = '⑩';

                    foreach (range(0, $data['maxx']) as $x) {
                        echo "<tr>";
                        foreach (range(0, $data['maxy']) as $y) {
                            echo "<td style=\"border: 0.5px solid #eeeeee;\">";
                            if (isset($ascii[$x][$y])) {
                                if ($ascii[$x][$y] == "═") {
                                    echo "<span class=\"stretch\">";
                                } else if ($ascii[$x][$y] == "│" || $ascii[$x][$y] == "║") {
                                    echo "<span class=\"stretchv\">";
                                } else if ($ascii[$x][$y] > 0) {
                                    $color = ((substr($ascii[$x][$y], 1) == $data['select']) ? " text-decoration: none; color: #ff0000;" : "none");

                                    echo "<a style=\"" . $color . "\" href=\"index.php?section=api&ville=" . substr($ascii[$x][$y], 1) . "\">";

                                }
                                echo ($ascii[$x][$y] > 0) ? $cities[intval($ascii[$x][$y][0])] : $ascii[$x][$y];
                                if ($ascii[$x][$y] > 0) {
                                    echo "</a>";
                                }
                                echo "</span>";

                            } else {
                                echo "&nbsp;&nbsp;&nbsp;&nbsp;";
                            }
                            echo "</td>";
                        }
                        echo "</tr>";
                    }
                }
                ?>
                </table>
            </div>
        </div>
        <br/><br/>
            <?php echo '<h3>Statut de la partie : ' . $data['erreur'] . '</h3>'; ?>
    </body>
</html>
<?php

}
}
?>