<?php 

// vue affichant le classement des joueurs selon leur statistiques
class VueClassement
{

  // affiche le classement
  // précondition: data contient les statistiques des joueurs
  function printHTML(array $data)
  {

    header('Content-type: text/html; charset=utf-8');
    ?>
<html>

<head>
  <title>Bridge- Classement</title>
  <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Cherry+Swash'>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
    crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="style/css/styleCl.css">
  <link rel="icon" href="style/image/BridgeFavicon.png" />
</head>

<body>
  <style>
    body {
      background: url("style/image/Savin-NY-Website-Background-Web.jpg") no-repeat center center fixed;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
    }
  </style>

  <div class="container-fluid">
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="index.php">Accueil</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php?section=tablejeu">Jouer</a></li>
            <li class="active"><a href="index.php?section=classement">Classement</a></li>

          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php?section=logout">Deconnexion</a></li>
          </ul>
        </div>
        <!--/.nav-collapse -->
      </div>
    </nav>
  </div>
  <br />
  <br />
  <br />
  <br />
  <div class="container">
    <h2>Classement</h2>
    <ul class="responsive-table">
      <li class="table-header">
        <div class="col col-1">Pseudo</div>
        <div class="col col-2">Ration Gagne/Perdu Name</div>
        <div class="col col-3">Parties Jouées</div>
        <div class="col col-4">Parties Gagnées</div>
      </li>
      <?php
      foreach ($data['infojoueurs'] as $joueur) {
        echo '<li class="table-row">';
        $ratio = (($joueur['partiesGagnees']) / ($joueur['partiesJouees']));
        echo '<div class="col col-1" data-label="Pseudo">' . $joueur['pseudo'] . '</div>';
        echo '<div class="col col-2" data-label="Ration Gagne/Perdu Name">' . round($ratio, 2) . '</div>';
        echo '<div class="col col-3" data-label="Parties Jouées">' . $joueur['partiesJouees'] . '</div>';
        echo '<div class="col col-4" data-label="Parties Gagnées">' . $joueur['partiesGagnees'] . '</div>';

        echo '</li>';
      }
      ?>
    </ul>
  </div>
</body>

</html>
<?php

}
}
?>