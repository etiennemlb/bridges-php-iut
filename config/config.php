<?php

// les chemins vers les différents répertoires liés au modèle MVC

// chemin complet sur le serveur de la racine du site, il est supposé que config.php est dans un sous-repertoire de la racine du site 
define("HOME_SITE", "/");

// définition des chemins vers les divers répertoires liés au modèle MVC
define("PATH_VUE", "vue");
define("PATH_CONTROLEUR", "controleur");
define("PATH_MODELE", "modele");
define("PATH_METIER", "metier");
define("PATH_STYLE", "style");


// données pour la connexion au sgbd
define("HOST", "localhost");
define("BD","E175028Y");
define("LOGIN","E175028Y");
define("PASSWORD","E175028Y");
?>
