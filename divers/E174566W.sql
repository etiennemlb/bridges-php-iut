-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 29 Novembre 2018 à 18:50
-- Version du serveur :  5.7.24-0ubuntu0.16.04.1
-- Version de PHP :  7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `E174566W`
--

DELIMITER $$
--
-- Procédures
--
CREATE DEFINER=`E174566W`@`%` PROCEDURE `add_user` (IN `lePseudo` VARCHAR(255), IN `lePassword` VARCHAR(255), IN `estAdmin` TINYINT(1) UNSIGNED, IN `accesseur` VARCHAR(255))  IF NOT EXISTS(SELECT pseudo FROM auth WHERE pseudo=lePseudo) THEN
INSERT INTO `E174566W`.`auth` (`id`, `pseudo`, `passw`) VALUES (NULL, lePseudo, lePassword);
COMMIT;
INSERT INTO `E174566W`.`users` VALUES ((SELECT id FROM auth WHERE pseudo=lePseudo),estAdmin,
                                      (SELECT id FROM accesseurs WHERE nomAccesseur=accesseur));
COMMIT;
END IF$$

CREATE DEFINER=`E174566W`@`%` PROCEDURE `delete_user` (IN `lePseudo` VARCHAR(255))  NO SQL
DELETE FROM auth WHERE pseudo=lePseudo$$

CREATE DEFINER=`E174566W`@`%` PROCEDURE `get_accesseur_from_pseudo` (IN `lePseudo` VARCHAR(255))  NO SQL
SELECT nomAccesseur FROM accesseurs WHERE id=(SELECT idAccesseur FROM users WHERE id=(SELECT id FROM auth WHERE pseudo=lePseudo))$$

CREATE DEFINER=`E174566W`@`%` PROCEDURE `is_admin` (IN `lePseudo` VARCHAR(255))  NO SQL
SELECT isAdmin FROM users WHERE id=(SELECT id FROM auth WHERE pseudo=lePseudo)$$

CREATE DEFINER=`E174566W`@`%` PROCEDURE `is_valid_session` (IN `lePseudo` VARCHAR(255), IN `lePassword` VARCHAR(255))  NO SQL
SELECT * FROM auth WHERE pseudo=lePseudo AND passw=lePassword$$

CREATE DEFINER=`E174566W`@`%` PROCEDURE `update_password_from_pseudo` (IN `lePseudo` VARCHAR(255), IN `mdp` VARCHAR(255))  NO SQL
UPDATE auth SET passw=mdp WHERE pseudo=lePseudo$$

CREATE DEFINER=`E174566W`@`%` PROCEDURE `update_pseudo_from_pseudo` (IN `ancienPseudo` VARCHAR(255), IN `nouveauPseudo` VARCHAR(255))  NO SQL
UPDATE auth SET pseudo=nouveauPseudo WHERE pseudo=ancienPseudo$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `accesseurs`
--

CREATE TABLE `accesseurs` (
  `id` int(11) NOT NULL,
  `nomAccesseur` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `accesseurs`
--

INSERT INTO `accesseurs` (`id`, `nomAccesseur`) VALUES
(4, 'acc_grp2_grille.php'),
(3, 'acc_grp2_static.php'),
(0, 'none.html'),
(2, 'test.html');

-- --------------------------------------------------------

--
-- Structure de la table `auth`
--

CREATE TABLE `auth` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `passw` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `auth`
--

INSERT INTO `auth` (`id`, `pseudo`, `passw`) VALUES
(33, '10MO', 'cdf5a998a91f135421cc5bdeb80ded1bf9e6e508f323b873adab77e4a37366263f6dc2b4192e8d1c79accfaec83b00b5a40169006aa5c9af474f45e21ac30108'),
(48, '5MO', 'e8d8f73bb87182daab8e65e1d1a6053b2f4a2a8e9fc123df9d1165a3349be274b6f7efeee5e7526d14f8b485d1940a6a1503f39074991605c12d244cff50a48f'),
(2, 'admin', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec'),
(18, 'Bart', 'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff'),
(44, 'etienne', 'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff'),
(47, 'execute', 'test'),
(29, 'Hello', '49532cf23c209c5ec9439ff0762073da6d48b9ce4856652016f0ddae8ad130fe690700cf146f9129ff70110a39a0509ea2ae8f323187cdd9f7008b3afbf58a9d'),
(19, 'Homer', '6e9656c3d405788434076fb0471af4f105821c77249eeac05988677b786ddc8b339cf40f69a891bcb1cb006283f7b839f754eb26d2bcfab3cb07de6ee7696c22'),
(32, 'leo', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec'),
(21, 'Maggie', 'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff'),
(20, 'Marge', '08856a9022cc1f4b7c90b2d059e64acb6f6c5ac11da907d86db6a3072e9d821c59603c1ea94a2e537bea0a38320d678c482a66eaaf1a79c4d3432ea41e51b721'),
(45, 'mememe', 'd1d3b000fff1cf85bfdef890f4d5eb0bca2be8ad4df7c3449a3a88a961032c3715b9ee9c02773f686a07407bf62c5320120251274885501e85d3c5a3765a0f50'),
(42, 'new truc', 'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff'),
(46, 'salutToi', 'test'),
(38, 'user_44b87', 'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff'),
(37, 'user_6cb89', 'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff'),
(39, 'user_aad53', 'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff'),
(41, 'user_e52f5', 'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff'),
(40, 'user_fd3aa', 'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff');

-- --------------------------------------------------------

--
-- Structure de la table `groupes`
--

CREATE TABLE `groupes` (
  `nom_groupe` varchar(255) NOT NULL,
  `identifiant_groupe` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `identification`
--

CREATE TABLE `identification` (
  `identifiant` int(11) NOT NULL,
  `login` varchar(36) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `identification`
--

INSERT INTO `identification` (`identifiant`, `login`, `password`) VALUES
(1, 'toto', '$6$RTRffX4m9FBU$GQPzOIuRByEJMeT8r9fydj8eKfi7yurb0SQiT./3pHnG5ni2f96gboxLE4LZgCgEVMWMP6z.AxaOM8KaWJCmn0'),
(2, 'titi', '$6$VsDCW/kqInRv$/bkDT4rmkNLGo704srZE1riI4u7IUUcSuuEqrdkeBJ.3RcsnEO.ihAnWvIWJ0fSoP3hVa/OpWTbhi50xQhzEk1');

-- --------------------------------------------------------

--
-- Structure de la table `joueurs`
--

CREATE TABLE `joueurs` (
  `pseudo` varchar(20) NOT NULL,
  `motDePasse` varchar(200) NOT NULL,
  `partiesJouees` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `partiesGagnees` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `joueurs`
--

INSERT INTO `joueurs` (`pseudo`, `motDePasse`, `partiesJouees`, `partiesGagnees`) VALUES
('titi', '$6$VsDCW/kqInRv$/bkDT4rmkNLGo704srZE1riI4u7IUUcSuuEqrdkeBJ.3RcsnEO.ihAnWvIWJ0fSoP3hVa/OpWTbhi50xQhzEk1', 39, 7),
('toto', '$6$RTRffX4m9FBU$GQPzOIuRByEJMeT8r9fydj8eKfi7yurb0SQiT./3pHnG5ni2f96gboxLE4LZgCgEVMWMP6z.AxaOM8KaWJCmn0', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `livres`
--

CREATE TABLE `livres` (
  `isbn` varchar(11) NOT NULL,
  `titre` varchar(30) NOT NULL,
  `auteur` varchar(30) NOT NULL,
  `editeur` varchar(30) NOT NULL,
  `image` varchar(70) NOT NULL,
  `prix` float NOT NULL,
  `quantite` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `livres`
--

INSERT INTO `livres` (`isbn`, `titre`, `auteur`, `editeur`, `image`, `prix`, `quantite`) VALUES
('2-876-54301', 'Php7: cours et exercices', 'Jean Engels', 'Eyrolles', 'http://infoweb-ens/~jacquin-c/images/php7.jpg', 30.4, 23),
('2-875-54901', 'XML en concentré', 'Eliotte Rusty Harold', 'O\'Reilly', 'http://infoweb-ens/~jacquin-c/images/XML.jpg', 45, 10),
('2-975-74381', 'Programmation avec Node.js, Ex', 'Eric Savion', 'Eyrolles', 'http://infoweb-ens/~jacquin-c/images/Nodejs.jpg', 32, 6),
('2-978-44881', 'Pratique de CSS et Javascript', 'Eric Sarrion', 'O\'Reilly', 'http://infoweb-ens/~jacquin-c/images/CSSEtJavascript.jpg', 32, 5),
('2-938-44981', 'AngularJS', 'Sébastien Ollivier', 'ENI', 'http://infoweb-ens/~jacquin-c/images/angularjs.jpg', 39.9, 10),
('2-938-44781', 'Débuter en javascript', 'Shelley Powers', 'Eyrolles', 'http://infoweb-ens/~jacquin-c/images/debuterEnJavascript.jpg', 29.9, 10);

-- --------------------------------------------------------

--
-- Structure de la table `pseudonyme`
--

CREATE TABLE `pseudonyme` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pseudonyme`
--

INSERT INTO `pseudonyme` (`id`, `pseudo`) VALUES
(1, 'lebofred'),
(2, 'titi44');

-- --------------------------------------------------------

--
-- Structure de la table `salon`
--

CREATE TABLE `salon` (
  `id` int(11) NOT NULL,
  `idpseudo` int(11) NOT NULL,
  `message` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `salon`
--

INSERT INTO `salon` (`id`, `idpseudo`, `message`) VALUES
(1, 1, 'coucou, je suis lebofred 0'),
(2, 2, 'coucou, je suis titi44 0'),
(3, 1, 'coucou, je suis lebofred 1'),
(4, 2, 'coucou, je suis titi44 1'),
(5, 1, 'coucou, je suis lebofred 2'),
(6, 2, 'coucou, je suis titi44 2'),
(7, 1, 'coucou, je suis lebofred 3'),
(8, 2, 'coucou, je suis titi44 3'),
(9, 1, 'coucou, je suis lebofred 4'),
(10, 2, 'coucou, je suis titi44 4'),
(11, 1, 'coucou, je suis lebofred 5'),
(12, 2, 'coucou, je suis titi44 5'),
(13, 1, 'coucou, je suis lebofred 0'),
(14, 2, 'coucou, je suis titi44 0'),
(15, 1, 'coucou, je suis lebofred 1'),
(16, 2, 'coucou, je suis titi44 1'),
(17, 1, 'coucou, je suis lebofred 2'),
(18, 2, 'coucou, je suis titi44 2'),
(19, 1, 'coucou, je suis lebofred 3'),
(20, 2, 'coucou, je suis titi44 3'),
(21, 1, 'coucou, je suis lebofred 4'),
(22, 2, 'coucou, je suis titi44 4'),
(23, 1, 'coucou, je suis lebofred 5'),
(24, 2, 'coucou, je suis titi44 5'),
(25, 1, 'coucou, je suis lebofred 0'),
(26, 2, 'coucou, je suis titi44 0'),
(27, 1, 'coucou, je suis lebofred 1'),
(28, 2, 'coucou, je suis titi44 1'),
(29, 1, 'coucou, je suis lebofred 2'),
(30, 2, 'coucou, je suis titi44 2'),
(31, 1, 'coucou, je suis lebofred 3'),
(32, 2, 'coucou, je suis titi44 3'),
(33, 1, 'coucou, je suis lebofred 4'),
(34, 2, 'coucou, je suis titi44 4'),
(35, 1, 'coucou, je suis lebofred 5'),
(36, 2, 'coucou, je suis titi44 5'),
(37, 1, 'coucou, je suis lebofred 0'),
(38, 2, 'coucou, je suis titi44 0'),
(39, 1, 'coucou, je suis lebofred 1'),
(40, 2, 'coucou, je suis titi44 1'),
(41, 1, 'coucou, je suis lebofred 2'),
(42, 2, 'coucou, je suis titi44 2'),
(43, 1, 'coucou, je suis lebofred 3'),
(44, 2, 'coucou, je suis titi44 3'),
(45, 1, 'coucou, je suis lebofred 4'),
(46, 2, 'coucou, je suis titi44 4'),
(47, 1, 'coucou, je suis lebofred 5'),
(48, 2, 'coucou, je suis titi44 5'),
(49, 1, 'coucou, je suis lebofred 0'),
(50, 2, 'coucou, je suis titi44 0'),
(51, 1, 'coucou, je suis lebofred 1'),
(52, 2, 'coucou, je suis titi44 1'),
(53, 1, 'coucou, je suis lebofred 2'),
(54, 2, 'coucou, je suis titi44 2'),
(55, 1, 'coucou, je suis lebofred 3'),
(56, 2, 'coucou, je suis titi44 3'),
(57, 1, 'coucou, je suis lebofred 4'),
(58, 2, 'coucou, je suis titi44 4'),
(59, 1, 'coucou, je suis lebofred 5'),
(60, 2, 'coucou, je suis titi44 5'),
(61, 1, 'coucou, je suis lebofred 0'),
(62, 2, 'coucou, je suis titi44 0'),
(63, 1, 'coucou, je suis lebofred 1'),
(64, 2, 'coucou, je suis titi44 1'),
(65, 1, 'coucou, je suis lebofred 2'),
(66, 2, 'coucou, je suis titi44 2'),
(67, 1, 'coucou, je suis lebofred 3'),
(68, 2, 'coucou, je suis titi44 3'),
(69, 1, 'coucou, je suis lebofred 4'),
(70, 2, 'coucou, je suis titi44 4'),
(71, 1, 'coucou, je suis lebofred 5'),
(72, 2, 'coucou, je suis titi44 5'),
(73, 1, 'coucou, je suis lebofred 0'),
(74, 2, 'coucou, je suis titi44 0'),
(75, 1, 'coucou, je suis lebofred 1'),
(76, 2, 'coucou, je suis titi44 1'),
(77, 1, 'coucou, je suis lebofred 2'),
(78, 2, 'coucou, je suis titi44 2'),
(79, 1, 'coucou, je suis lebofred 3'),
(80, 2, 'coucou, je suis titi44 3'),
(81, 1, 'coucou, je suis lebofred 4'),
(82, 2, 'coucou, je suis titi44 4'),
(83, 1, 'coucou, je suis lebofred 5'),
(84, 2, 'coucou, je suis titi44 5'),
(85, 1, 'coucou, je suis lebofred 0'),
(86, 2, 'coucou, je suis titi44 0'),
(87, 1, 'coucou, je suis lebofred 1'),
(88, 2, 'coucou, je suis titi44 1'),
(89, 1, 'coucou, je suis lebofred 2'),
(90, 2, 'coucou, je suis titi44 2'),
(91, 1, 'coucou, je suis lebofred 3'),
(92, 2, 'coucou, je suis titi44 3'),
(93, 1, 'coucou, je suis lebofred 4'),
(94, 2, 'coucou, je suis titi44 4'),
(95, 1, 'coucou, je suis lebofred 5'),
(96, 2, 'coucou, je suis titi44 5'),
(97, 1, 'coucou, je suis lebofred 0'),
(98, 2, 'coucou, je suis titi44 0'),
(99, 1, 'coucou, je suis lebofred 1'),
(100, 2, 'coucou, je suis titi44 1'),
(101, 1, 'coucou, je suis lebofred 2'),
(102, 2, 'coucou, je suis titi44 2'),
(103, 1, 'coucou, je suis lebofred 3'),
(104, 2, 'coucou, je suis titi44 3'),
(105, 1, 'coucou, je suis lebofred 4'),
(106, 2, 'coucou, je suis titi44 4'),
(107, 1, 'coucou, je suis lebofred 5'),
(108, 2, 'coucou, je suis titi44 5'),
(109, 1, 'coucou, je suis lebofred 0'),
(110, 2, 'coucou, je suis titi44 0'),
(111, 1, 'coucou, je suis lebofred 1'),
(112, 2, 'coucou, je suis titi44 1'),
(113, 1, 'coucou, je suis lebofred 2'),
(114, 2, 'coucou, je suis titi44 2'),
(115, 1, 'coucou, je suis lebofred 3'),
(116, 2, 'coucou, je suis titi44 3'),
(117, 1, 'coucou, je suis lebofred 4'),
(118, 2, 'coucou, je suis titi44 4'),
(119, 1, 'coucou, je suis lebofred 5'),
(120, 2, 'coucou, je suis titi44 5'),
(121, 1, 'coucou, je suis lebofred 0'),
(122, 2, 'coucou, je suis titi44 0'),
(123, 1, 'coucou, je suis lebofred 1'),
(124, 2, 'coucou, je suis titi44 1'),
(125, 1, 'coucou, je suis lebofred 2'),
(126, 2, 'coucou, je suis titi44 2'),
(127, 1, 'coucou, je suis lebofred 3'),
(128, 2, 'coucou, je suis titi44 3'),
(129, 1, 'coucou, je suis lebofred 4'),
(130, 2, 'coucou, je suis titi44 4'),
(131, 1, 'coucou, je suis lebofred 5'),
(132, 2, 'coucou, je suis titi44 5'),
(133, 1, 'coucou, je suis lebofred 0'),
(134, 2, 'coucou, je suis titi44 0'),
(135, 1, 'coucou, je suis lebofred 1'),
(136, 2, 'coucou, je suis titi44 1'),
(137, 1, 'coucou, je suis lebofred 2'),
(138, 2, 'coucou, je suis titi44 2'),
(139, 1, 'coucou, je suis lebofred 3'),
(140, 2, 'coucou, je suis titi44 3'),
(141, 1, 'coucou, je suis lebofred 4'),
(142, 2, 'coucou, je suis titi44 4'),
(143, 1, 'coucou, je suis lebofred 5'),
(144, 2, 'coucou, je suis titi44 5'),
(145, 1, 'coucou, je suis lebofred 0'),
(146, 2, 'coucou, je suis titi44 0'),
(147, 1, 'coucou, je suis lebofred 1'),
(148, 2, 'coucou, je suis titi44 1'),
(149, 1, 'coucou, je suis lebofred 2'),
(150, 2, 'coucou, je suis titi44 2'),
(151, 1, 'coucou, je suis lebofred 3'),
(152, 2, 'coucou, je suis titi44 3'),
(153, 1, 'coucou, je suis lebofred 4'),
(154, 2, 'coucou, je suis titi44 4'),
(155, 1, 'coucou, je suis lebofred 5'),
(156, 2, 'coucou, je suis titi44 5'),
(157, 1, 'coucou, je suis lebofred 0'),
(158, 2, 'coucou, je suis titi44 0'),
(159, 1, 'coucou, je suis lebofred 1'),
(160, 2, 'coucou, je suis titi44 1'),
(161, 1, 'coucou, je suis lebofred 2'),
(162, 2, 'coucou, je suis titi44 2'),
(163, 1, 'coucou, je suis lebofred 3'),
(164, 2, 'coucou, je suis titi44 3'),
(165, 1, 'coucou, je suis lebofred 4'),
(166, 2, 'coucou, je suis titi44 4'),
(167, 1, 'coucou, je suis lebofred 5'),
(168, 2, 'coucou, je suis titi44 5'),
(169, 1, 'coucou, je suis lebofred 0'),
(170, 2, 'coucou, je suis titi44 0'),
(171, 1, 'coucou, je suis lebofred 1'),
(172, 2, 'coucou, je suis titi44 1'),
(173, 1, 'coucou, je suis lebofred 2'),
(174, 2, 'coucou, je suis titi44 2'),
(175, 1, 'coucou, je suis lebofred 3'),
(176, 2, 'coucou, je suis titi44 3'),
(177, 1, 'coucou, je suis lebofred 4'),
(178, 2, 'coucou, je suis titi44 4'),
(179, 1, 'coucou, je suis lebofred 5'),
(180, 2, 'coucou, je suis titi44 5'),
(181, 1, 'coucou, je suis lebofred 0'),
(182, 2, 'coucou, je suis titi44 0'),
(183, 1, 'coucou, je suis lebofred 1'),
(184, 2, 'coucou, je suis titi44 1'),
(185, 1, 'coucou, je suis lebofred 2'),
(186, 2, 'coucou, je suis titi44 2'),
(187, 1, 'coucou, je suis lebofred 3'),
(188, 2, 'coucou, je suis titi44 3'),
(189, 1, 'coucou, je suis lebofred 4'),
(190, 2, 'coucou, je suis titi44 4'),
(191, 1, 'coucou, je suis lebofred 5'),
(192, 2, 'coucou, je suis titi44 5'),
(193, 1, 'coucou, je suis lebofred 0'),
(194, 2, 'coucou, je suis titi44 0'),
(195, 1, 'coucou, je suis lebofred 1'),
(196, 2, 'coucou, je suis titi44 1'),
(197, 1, 'coucou, je suis lebofred 2'),
(198, 2, 'coucou, je suis titi44 2'),
(199, 1, 'coucou, je suis lebofred 3'),
(200, 2, 'coucou, je suis titi44 3'),
(201, 1, 'coucou, je suis lebofred 4'),
(202, 2, 'coucou, je suis titi44 4'),
(203, 1, 'coucou, je suis lebofred 5'),
(204, 2, 'coucou, je suis titi44 5'),
(205, 1, 'coucou, je suis lebofred 0'),
(206, 2, 'coucou, je suis titi44 0'),
(207, 1, 'coucou, je suis lebofred 1'),
(208, 2, 'coucou, je suis titi44 1'),
(209, 1, 'coucou, je suis lebofred 2'),
(210, 2, 'coucou, je suis titi44 2'),
(211, 1, 'coucou, je suis lebofred 3'),
(212, 2, 'coucou, je suis titi44 3'),
(213, 1, 'coucou, je suis lebofred 4'),
(214, 2, 'coucou, je suis titi44 4'),
(215, 1, 'coucou, je suis lebofred 5'),
(216, 2, 'coucou, je suis titi44 5'),
(217, 1, 'coucou, je suis lebofred 0'),
(218, 2, 'coucou, je suis titi44 0'),
(219, 1, 'coucou, je suis lebofred 1'),
(220, 2, 'coucou, je suis titi44 1'),
(221, 1, 'coucou, je suis lebofred 2'),
(222, 2, 'coucou, je suis titi44 2'),
(223, 1, 'coucou, je suis lebofred 3'),
(224, 2, 'coucou, je suis titi44 3'),
(225, 1, 'coucou, je suis lebofred 4'),
(226, 2, 'coucou, je suis titi44 4'),
(227, 1, 'coucou, je suis lebofred 5'),
(228, 2, 'coucou, je suis titi44 5'),
(229, 1, 'coucou, je suis lebofred 0'),
(230, 2, 'coucou, je suis titi44 0'),
(231, 1, 'coucou, je suis lebofred 1'),
(232, 2, 'coucou, je suis titi44 1'),
(233, 1, 'coucou, je suis lebofred 2'),
(234, 2, 'coucou, je suis titi44 2'),
(235, 1, 'coucou, je suis lebofred 3'),
(236, 2, 'coucou, je suis titi44 3'),
(237, 1, 'coucou, je suis lebofred 4'),
(238, 2, 'coucou, je suis titi44 4'),
(239, 1, 'coucou, je suis lebofred 5'),
(240, 2, 'coucou, je suis titi44 5'),
(241, 1, 'coucou, je suis lebofred 0'),
(242, 2, 'coucou, je suis titi44 0'),
(243, 1, 'coucou, je suis lebofred 1'),
(244, 2, 'coucou, je suis titi44 1'),
(245, 1, 'coucou, je suis lebofred 2'),
(246, 2, 'coucou, je suis titi44 2'),
(247, 1, 'coucou, je suis lebofred 3'),
(248, 2, 'coucou, je suis titi44 3'),
(249, 1, 'coucou, je suis lebofred 4'),
(250, 2, 'coucou, je suis titi44 4'),
(251, 1, 'coucou, je suis lebofred 5'),
(252, 2, 'coucou, je suis titi44 5'),
(253, 1, 'coucou, je suis lebofred 0'),
(254, 2, 'coucou, je suis titi44 0'),
(255, 1, 'coucou, je suis lebofred 1'),
(256, 2, 'coucou, je suis titi44 1'),
(257, 1, 'coucou, je suis lebofred 2'),
(258, 2, 'coucou, je suis titi44 2'),
(259, 1, 'coucou, je suis lebofred 3'),
(260, 2, 'coucou, je suis titi44 3'),
(261, 1, 'coucou, je suis lebofred 4'),
(262, 2, 'coucou, je suis titi44 4'),
(263, 1, 'coucou, je suis lebofred 5'),
(264, 2, 'coucou, je suis titi44 5'),
(265, 1, 'coucou, je suis lebofred 0'),
(266, 2, 'coucou, je suis titi44 0'),
(267, 1, 'coucou, je suis lebofred 1'),
(268, 2, 'coucou, je suis titi44 1'),
(269, 1, 'coucou, je suis lebofred 2'),
(270, 2, 'coucou, je suis titi44 2'),
(271, 1, 'coucou, je suis lebofred 3'),
(272, 2, 'coucou, je suis titi44 3'),
(273, 1, 'coucou, je suis lebofred 4'),
(274, 2, 'coucou, je suis titi44 4'),
(275, 1, 'coucou, je suis lebofred 5'),
(276, 2, 'coucou, je suis titi44 5'),
(277, 1, 'coucou, je suis lebofred 0'),
(278, 2, 'coucou, je suis titi44 0'),
(279, 1, 'coucou, je suis lebofred 1'),
(280, 2, 'coucou, je suis titi44 1'),
(281, 1, 'coucou, je suis lebofred 2'),
(282, 2, 'coucou, je suis titi44 2'),
(283, 1, 'coucou, je suis lebofred 3'),
(284, 2, 'coucou, je suis titi44 3'),
(285, 1, 'coucou, je suis lebofred 4'),
(286, 2, 'coucou, je suis titi44 4'),
(287, 1, 'coucou, je suis lebofred 5'),
(288, 2, 'coucou, je suis titi44 5'),
(289, 1, 'coucou, je suis lebofred 0'),
(290, 2, 'coucou, je suis titi44 0'),
(291, 1, 'coucou, je suis lebofred 1'),
(292, 2, 'coucou, je suis titi44 1'),
(293, 1, 'coucou, je suis lebofred 2'),
(294, 2, 'coucou, je suis titi44 2'),
(295, 1, 'coucou, je suis lebofred 3'),
(296, 2, 'coucou, je suis titi44 3'),
(297, 1, 'coucou, je suis lebofred 4'),
(298, 2, 'coucou, je suis titi44 4'),
(299, 1, 'coucou, je suis lebofred 5'),
(300, 2, 'coucou, je suis titi44 5'),
(301, 1, 'coucou, je suis lebofred 0'),
(302, 2, 'coucou, je suis titi44 0'),
(303, 1, 'coucou, je suis lebofred 1'),
(304, 2, 'coucou, je suis titi44 1'),
(305, 1, 'coucou, je suis lebofred 2'),
(306, 2, 'coucou, je suis titi44 2'),
(307, 1, 'coucou, je suis lebofred 3'),
(308, 2, 'coucou, je suis titi44 3'),
(309, 1, 'coucou, je suis lebofred 4'),
(310, 2, 'coucou, je suis titi44 4'),
(311, 1, 'coucou, je suis lebofred 5'),
(312, 2, 'coucou, je suis titi44 5'),
(313, 1, 'coucou, je suis lebofred 0'),
(314, 2, 'coucou, je suis titi44 0'),
(315, 1, 'coucou, je suis lebofred 1'),
(316, 2, 'coucou, je suis titi44 1'),
(317, 1, 'coucou, je suis lebofred 2'),
(318, 2, 'coucou, je suis titi44 2'),
(319, 1, 'coucou, je suis lebofred 3'),
(320, 2, 'coucou, je suis titi44 3'),
(321, 1, 'coucou, je suis lebofred 4'),
(322, 2, 'coucou, je suis titi44 4'),
(323, 1, 'coucou, je suis lebofred 5'),
(324, 2, 'coucou, je suis titi44 5'),
(325, 1, 'coucou, je suis lebofred 0'),
(326, 2, 'coucou, je suis titi44 0'),
(327, 1, 'coucou, je suis lebofred 1'),
(328, 2, 'coucou, je suis titi44 1'),
(329, 1, 'coucou, je suis lebofred 2'),
(330, 2, 'coucou, je suis titi44 2'),
(331, 1, 'coucou, je suis lebofred 3'),
(332, 2, 'coucou, je suis titi44 3'),
(333, 1, 'coucou, je suis lebofred 4'),
(334, 2, 'coucou, je suis titi44 4'),
(335, 1, 'coucou, je suis lebofred 5'),
(336, 2, 'coucou, je suis titi44 5'),
(337, 1, 'coucou, je suis lebofred 0'),
(338, 2, 'coucou, je suis titi44 0'),
(339, 1, 'coucou, je suis lebofred 1'),
(340, 2, 'coucou, je suis titi44 1'),
(341, 1, 'coucou, je suis lebofred 2'),
(342, 2, 'coucou, je suis titi44 2'),
(343, 1, 'coucou, je suis lebofred 3'),
(344, 2, 'coucou, je suis titi44 3'),
(345, 1, 'coucou, je suis lebofred 4'),
(346, 2, 'coucou, je suis titi44 4'),
(347, 1, 'coucou, je suis lebofred 5'),
(348, 2, 'coucou, je suis titi44 5'),
(349, 2, 'sqzerfqsf'),
(350, 2, 'qsfqsdf'),
(351, 2, 'qsdfqsdf'),
(352, 2, 'qsdfqsdf'),
(353, 2, 'qsdfqsdf'),
(354, 2, 'qsdfqsdf'),
(355, 2, 'qsdfqsdf'),
(356, 2, '<script>window.location="http://infoweb.iut-nantes.univ-nantes.prive/~e174566w/TP_5/index.php"<scrip'),
(357, 2, 'aze'),
(358, 2, 'qwgqwg'),
(359, 2, 'sdgh'),
(360, 2, 'dfh'),
(361, 2, 'fhj'),
(362, 2, 'fgjk'),
(363, 2, 'dfh'),
(364, 2, 'sdfg'),
(365, 2, 'sdfh'),
(366, 2, 'xfgh'),
(367, 2, 'cvh'),
(368, 2, 'dfh'),
(369, 2, '<script>document.cookie</script>'),
(370, 2, '<script>console.log(document.cookie)</script>'),
(371, 2, '<sc'),
(372, 2, '<script>console.log(document.cookie)</script>'),
(373, 2, 'fghj'),
(374, 2, 'qsdf'),
(375, 2, 'dfg'),
(376, 2, 'cfg'),
(377, 2, ''),
(378, 2, 'wfdg'),
(379, 2, 'sdfg'),
(380, 2, 'qsdfg'),
(381, 2, 'qsf'),
(382, 2, 'qsdfg'),
(383, 2, 'ert'),
(384, 2, 'ezrt'),
(385, 2, 'zert'),
(386, 2, 'zert'),
(387, 2, 'zert'),
(388, 2, 'zert'),
(389, 2, 'zert'),
(390, 2, 'zert'),
(391, 2, '<h1 id="groovy">Pwned</h1>'),
(392, 2, '<h1 id="groovy">Pwned</h1>'),
(393, 2, '<h1 id="groovy">Pwned</h1>'),
(394, 2, '<h1 id="groovy">Pwned</h1>');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `idAccesseur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `isAdmin`, `idAccesseur`) VALUES
(2, 1, 0),
(18, 0, 0),
(19, 0, 2),
(20, 0, 0),
(21, 0, 0),
(29, 0, 0),
(32, 0, 0),
(33, 0, 3),
(37, 0, 2),
(38, 0, 0),
(39, 0, 0),
(40, 0, 0),
(41, 0, 0),
(42, 0, 0),
(44, 0, 0),
(45, 0, 0),
(46, 0, 2),
(47, 0, 2),
(48, 0, 2);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `accesseurs`
--
ALTER TABLE `accesseurs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nomAccesseur` (`nomAccesseur`);

--
-- Index pour la table `auth`
--
ALTER TABLE `auth`
  ADD PRIMARY KEY (`id`) COMMENT='id du couple pseudo/password',
  ADD UNIQUE KEY `pseudo` (`pseudo`,`passw`),
  ADD KEY `pseudo_2` (`pseudo`,`passw`);

--
-- Index pour la table `groupes`
--
ALTER TABLE `groupes`
  ADD PRIMARY KEY (`nom_groupe`) COMMENT='Clé primaire';

--
-- Index pour la table `identification`
--
ALTER TABLE `identification`
  ADD PRIMARY KEY (`identifiant`);

--
-- Index pour la table `joueurs`
--
ALTER TABLE `joueurs`
  ADD PRIMARY KEY (`pseudo`);

--
-- Index pour la table `livres`
--
ALTER TABLE `livres`
  ADD PRIMARY KEY (`isbn`);

--
-- Index pour la table `pseudonyme`
--
ALTER TABLE `pseudonyme`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pseudo` (`pseudo`);

--
-- Index pour la table `salon`
--
ALTER TABLE `salon`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idpseudo` (`idpseudo`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `indexid` (`id`),
  ADD KEY `idAccesseur` (`idAccesseur`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `accesseurs`
--
ALTER TABLE `accesseurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `auth`
--
ALTER TABLE `auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT pour la table `identification`
--
ALTER TABLE `identification`
  MODIFY `identifiant` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `pseudonyme`
--
ALTER TABLE `pseudonyme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `salon`
--
ALTER TABLE `salon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=395;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `salon`
--
ALTER TABLE `salon`
  ADD CONSTRAINT `salon_ibfk_2` FOREIGN KEY (`idpseudo`) REFERENCES `pseudonyme` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_id_accesseur` FOREIGN KEY (`idAccesseur`) REFERENCES `accesseurs` (`id`),
  ADD CONSTRAINT `fk_id_user` FOREIGN KEY (`id`) REFERENCES `auth` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
