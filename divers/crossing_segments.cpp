#include <iostream>
#include <string>

template <typename T>
struct point {
  T x;
  T y;

  point(T ix, T iy) : x(ix), y(iy) {}

  bool operator==(const point<T>& b) const noexcept {
    return x == b.x && y == b.y;
  }

  bool operator!=(const point<T>& b) const noexcept { return !(*this == b); }

  std::string toString() { return std::to_string(x) + "," + std::to_string(y); }
};

using point2I = point<int32_t>;

bool isCrossing(point2I* a, point2I* b, point2I* c, point2I* d) {
  bool __a = a->x < c->x && a->x < d->x;
  bool __b = b->x < c->x && b->x < d->x;

  bool a__ = a->x > c->x && a->x > d->x;
  bool b__ = b->x > c->x && b->x > d->x;

  bool upA = a->y < c->y && a->y < d->y;
  bool upB = b->y < c->y && b->y < d->y;

  bool downA = a->y > c->y && a->y > d->y;
  bool downB = b->y > c->y && b->y > d->y;

  return !((__a && __b) || (a__ && b__) || (upA && upB) || (downA && downB));
}


int main(int argc, char* argv[]) {
  point2I *a = nullptr, *b = nullptr, *c = nullptr, *d = nullptr;

  if (argc < 9)
    return -1;
  a = new point2I(std::atoi(argv[1]), std::atoi(argv[2]));
  b = new point2I(std::atoi(argv[3]), std::atoi(argv[4]));
  c = new point2I(std::atoi(argv[5]), std::atoi(argv[6]));
  d = new point2I(std::atoi(argv[7]), std::atoi(argv[8]));

  std::cout << "A : " << a->toString() << "\nB : " << b->toString()
            << "\nC : " << c->toString() << "\nD : " << d->toString() << '\n';

  std::cout << "Is crossing ? (a,b,c,d): " << isCrossing(a, b, c, d) << '\n';
  std::cout << "Is crossing ? (b,a,d,c): " << isCrossing(b, a, d, c) << '\n';
  std::cout << "Is crossing ? (c,d,a,b): " << isCrossing(c, d, a, b) << '\n';
  std::cout << "Is crossing ? (d,c,b,a): " << isCrossing(d, c, b, a)
            << std::endl;
  return 0;
}