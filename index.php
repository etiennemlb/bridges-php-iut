<?php
require_once("config/global.php");
require_once("config/config.php");
require_once(PATH_CONTROLEUR . "/routeur.php");

try {
    // appel au routeur
    $routeur = new Routeur();
    $routeur->routerRequete();

} catch (Exception $e) {

    // en cas de problème, on affiche une vue d'erreur à l'utilisateur
    require_once(PATH_VUE . '/vueErreurServeur.php');
    $vueErreur = new VueErreur();
    $vueErreur->printHTML(array('erreur' => $e));

}
?>