<?php
require_once('Villes.php');
require_once('Action.php');

// classe modèle représentant une partie et son état
class Partie
{

    private $villes; // la liste des villes de la partie
    private $actions; // la pile des actions effectuées par le joueur
    private $ponts; // la liste des ponts crées
    private $villeselect; // la ville actuellement sélectionnée par le joueur

    // initialise une nouvelle partie
    function __construct()
    {
        $this->villes = new Villes();
        $this->ponts = array();
        $this->actions = array();
        $this->villeselect = null;
    }
        
    // selectionne une ville
    function selectionnerVille(VilleNF $ville)
    {
        $this->villeselect = $ville;
    }

    // retourne la ville selectionnee
    function villeSelect()
    {
        return $this->villeselect;
    }

    // déselectionne la ville
    function deselectVille()
    {
        $this->villeselect = null;
    }

    // retourne true si 
    function isVilleSelect()
    {
        return $this->villeselect != null;
    }

    // ajoute une action à la listes des actions effectuées par l'utilisateur
    function ajouterAction($type, VilleNF $ville1, VilleNF $ville2)
    {
        array_push($this->actions, new Action($type, $ville1, $ville2));
    }

    // annule la dernière action effectuée
    function annulerAction()
    {
        array_pop($this->actions);
    }

    // remettre à jour la liste des ponts de la partie en fonction de la succession des actions effectuées et enregistrées
    function mettreAJour()
    {
        foreach ($this->villes->getVilles() as $ville) {
            $ville->setNombrePonts(0);
        }
        // on supprime les ponts qui étaient enregistrés
        $this->ponts = array();
        
        // on éxécute toutes les actions les unes après les autres
        foreach ($this->actions as $action) {
            $action->executer($this);
        }
    }

    // retourne un pont concernant deux villes définies. Retourne null si aucun pont n'existe
    function rechercherPont(VilleNF $ville1, VilleNF $ville2)
    {
        foreach ($this->ponts as $pont) {

            // le sens du pont n'a pas d'importance
            $a = $pont->ville1()->getId() == $ville1->getId() && $pont->ville2()->getId() == $ville2->getId();
            $b = $pont->ville2()->getId() == $ville1->getId() && $pont->ville1()->getId() == $ville2->getId();

            if ($a or $b) {
                return $pont;
            }
        }
        return null;
    }

    // ajoute un pont (si un pont existe déjà, incrémente le nombre de ponts entre les deux villes)
    function creerPont(VilleNF $ville1, VilleNF $ville2)
    {
        $pont = $this->rechercherPont($ville1, $ville2);

        if ($pont != null) {
            $pont->ajouterPont();

            $pont->ville1()->addNombrePonts(1);
            $pont->ville2()->addNombrePonts(1);
        } else {

            $p = new Pont($ville1, $ville2);
            $p->ville1()->addNombrePonts(1);
            $p->ville2()->addNombrePonts(1);
            array_push($this->ponts, $p);
        }
    }

    // supprime un pont
    function supprimerPont(VilleNF $ville1, VilleNF $ville2)
    {
        $pont = $this->rechercherPont($ville1, $ville2);

        $key = array_search($pont, $this->ponts);

        $this->ponts[$key]->ville1()->addNombrePonts(-2);
        $this->ponts[$key]->ville2()->addNombrePonts(-2);

        unset($this->ponts[$key]);
    }

    // retourne l'objet contenant la liste des villes
    function villes()
    {
        return $this->villes;
    }

    // retourne la liste des ponts créés
    function ponts()
    {
        return $this->ponts;
    }

    // retourne la liste des actions de l'utilisateur
    function actions()
    {
        return $this->actions;
    }

    // retourne un pont entre les deux villes si un pont peut être créé entre les
    // deux villes passées en paramètre en respectant les
    // contraintes du jeu (alignement des villes, pas de ville entre elles, pas de pont entre elles)
    // sinon, retourne null
    function pontPossible(VilleNF $ville1, VilleNF $ville2)
    {

        // tester si les villes sont alignées sur la grille
        $aligne = $ville1->aligne($ville2);

        if (!$aligne)
            return null;
        

        // tester si il existe un pont entre les villes
        $pont = new Pont($ville1, $ville2);

        $collision = false;
        foreach ($this->ponts as $p) {
            if (!(($pont->ville1() == $p->ville1() && $pont->ville2() == $p->ville2())
                || ($pont->ville1() == $p->ville2() && $pont->ville2() == $p->ville1()))

                && $pont->collision($p)) {
                $collision = true;
                break;
            }
        }

        // tester si il y a une ville entre les deux villes
        $villeEntre = false;
        // si les villes sont alignées verticalement
        if ($ville1->getX() == $ville2->getX()) {
            $x = $ville1->getX();
            $ymin = min($ville1->getY(), $ville2->getY());
            $ymax = max($ville1->getY(), $ville2->getY());

            foreach (range($ymin + 1, $ymax - 1) as $i) {
                if ($this->villes->existe($x, $i)) {
                    $villeEntre = true;
                    break;
                }
            }
        // si les villes sont alignées horizontalement
        } else if ($ville1->getY() == $ville2->getY()) {
            $y = $ville1->getY();
            $xmin = min($ville1->getX(), $ville2->getX());
            $xmax = max($ville1->getX(), $ville2->getX());

            foreach (range($xmin + 1, $xmax - 1) as $i) {
                if ($this->villes->existe($i, $y)) {
                    $villeEntre = true;
                    break;
                }
            }
        }

        if (!$collision and !$villeEntre) {
            return $pont;
        } else {
            return null;
        }
    }

    // méthode retournant true si la partie est terminée et l'état du jeu correct, false sinon
    function partieGagne()
    {
        $a = $this->villes->testNombrePonts();
        $b = $this->villes->testConnexe($this->ponts);

        return $a && $b;
    }

    // méthode sérialisant la partie
    function serializeMoi()
    {
        return serialize($this);
    }

    function __wakeup()
    {
        $this->mettreAJour();
    }
}

?>