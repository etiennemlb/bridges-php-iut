<?php
require_once('Ville.php');

// classe représentant la liste des villes de la partie
class Villes
{
    private $villes;
    public $maxX;
    public $maxY;

    function __construct()
    {
        //#################################################
        // tableau représentatif d'un jeu qui servira à développer votre code

        $this->villes = array();
        
        $this->villes[0][0] = new Ville("0", 3, 0);
        $this->villes[0][6] = new Ville("1", 2, 0);
        $this->villes[3][0] = new Ville("2", 6, 0);
        $this->villes[3][5] = new Ville("3", 2, 0);
        $this->villes[5][1] = new Ville("4", 1, 0);
        $this->villes[5][6] = new Ville("5", 2, 0);
        $this->villes[6][0] = new Ville("6", 2, 0);
        
        //##################################################
        // Ne pas remplacer cette ligne 
        $this->transformationformat();
    }

    // méthode de compatibilité transformant le tableau de villes en deux dimensions en un tableau simple de villes contenant leur propres coordonnées
    function transformationformat()
    {
        $this->maxX = 0;
        $this->maxY = 0;
        $villesNF = array();

        for ($i = 0; $i < 1000; $i++) {
            if (isset($this->villes[$i])) $this->maxX = $i;
            for ($j = 0; $j < 1000; $j++) {
                if (isset($this->villes[$i][$j])) {
                    $this->maxY = max($this->maxY, $j);
                    array_push($villesNF, new VilleNF($this->villes[$i][$j]->id, $i, $j, $this->villes[$i][$j]->nombrePontsMax));
                }
            }
        }
        $this->villes = $villesNF;
    }

    // sélecteur qui retourne la ville en position $i 
    // précondition: la ville en position $i existe
    function getVille($i)
    {
        return $this->villes[$i];
    }

    // sélecteur permettant de récupérer une ville par son id
    // postcondition : retourne la ville correspondant à l'id, retourne null si l'id n'est attribué à aucune ville
    function getVilleAvecID($id)
    {
        foreach ($this->villes as $ville) {
            if ($ville->getId() == $id)
                return $ville;
        }
        return null;
    }

    // méthode retournant la liste des villes
    function getVilles()
    {
        return $this->villes;
    }

    // modifieur qui value le nombre de ponts de la ville en position $i et $j;
    // précondition: la ville en position $i et $j existe
    function setVille($i, $j, $nombrePonts)
    {
        $this->villes[$i][$j]->setNombrePonts($nombrePonts);
    }


    // permet de tester si la ville en position $x et $y existe 
    // postcondition: vrai si la ville existe, faux sinon
    function existe($x, $y)
    {
        foreach ($this->villes as $v) {
            if ($v->getX() == $x and $v->getY() == $y) {
                return true;
            }
        }
        return false;
    }

    // permet de savoir si toutes les villes de la partie 
    // sont connectées au bon nombre de ponts
    // - prend en paramètre la liste des ponts de la partie
    // retourne true si toutes les villes sont connectées à un nombre de ponts correcte, false sinon
    function testNombrePonts()
    {

        $correctnb = true;
        foreach ($this->villes as $v) {
            if ($v->getNombrePontsMax() != $v->getNombrePonts()) {
                        
                $correctnb = false;
                break;
            }
        }

        return $correctnb;
    }

    // permet de savoir si toutes le graphe des villes est connexe (toutes les villes sont reliées)
    // - prend en paramètre la liste des ponts de la partie
    // retourne true si toutes les villes sont connectées entre elles, false sinon
    function testConnexe($ponts)
    {

        $connexe = true;
        $villedepart = $this->villes[0];
        $villesatester = array();
        $villesconnexes = array();

        array_push($villesatester, $villedepart);

        while (!empty($villesatester) && count($villesconnexes) < count($this->villes)) {
            $v = array_pop($villesatester); // on prend une ville à tester

            // on teste les ponts qui concernent la ville et on push les villes de destinations des ponts
            $pts = array();
            foreach ($ponts as $pt) {
                if ($pt->ville1() == $v) {
                    $vl = $pt->ville2();
                    if (!in_array($vl, $villesconnexes)) {
                        array_push($villesatester, $vl);
                    }
                } else if ($pt->ville2() == $v) {
                    $vl = $pt->ville1();
                    if (!in_array($vl, $villesconnexes)) {
                        array_push($villesatester, $vl);
                    }
                }
            }

            array_push($villesconnexes, $v);
        }

        return count($villesconnexes) == count($this->villes);
    }
}
?>
