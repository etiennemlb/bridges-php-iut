<?php
require_once('Ville.php');

// classe représentant un pont
class Pont
{

    private $ville1, $ville2; // les deux villes concernant le pont
    private $count; // le nombre de ponts entre les deux villes
    private $horizontal; // = true si le pont est horizontal, false sinon

    // constructeur du pont
    function __construct(VilleNF $ville1, VilleNF $ville2)
    {
        $this->ville1 = $ville1;
        $this->ville2 = $ville2;
        $this->count = 1;
        $this->horizontal = ($ville1->getY() == $ville2->getY());
    }

    // ajoute un pont entre les deux villes
    // pré-condition : le nombre de ponts est inférieur à 2
    function ajouterPont()
    {
        $this->count++;
    }

    // retrourne le nombre de ponts entre les deux villes
    function nombrePonts()
    {
        return $this->count;
    }

    // retourne true si il ne peut pas y avoir plus de ponts entre les deux villes
    function maxPontsAtteint()
    {
        return $this->count >= 2;
    }

    // retourne la première ville concernée par le pont
    function ville1()
    {
        return $this->ville1;
    }

    // retourne la seconde ville concernée par le pont
    function ville2()
    {
        return $this->ville2;
    }

    // retourne true si le pont est horizontal
    function estHorizontal()
    {
        return $this->horizontal;
    }
        
    // retourne true si le pont p2 croise le pont, false sinon
    function collision(Pont $p2)
    {
        $x1 = $this->ville1->getX();
        $x2 = $this->ville2->getX();
        $y1 = $this->ville1->getY();
        $y2 = $this->ville2->getY();

        $x1o = $p2->ville1()->getX();
        $x2o = $p2->ville2()->getX();
        $y1o = $p2->ville1()->getY();
        $y2o = $p2->ville2()->getY();

        $__a = $x1 < $x1o && $x1 < $x2o;
        $__b = $x2 < $x1o && $x2 < $x2o;

        $a__ = $x1 > $x1o && $x1 > $x2o;
        $b__ = $x2 > $x1o && $x2 > $x2o;

        $upA = $y1 < $y1o && $y1 < $y2o;
        $upB = $y2 < $y1o && $y2 < $y2o;

        $downA = $y1 > $y1o && $y1 > $y2o;
        $downB = $y2 > $y1o && $y2 > $y2o;

        $final = !(($__a && $__b) || ($a__ && $b__) || ($upA && $upB) || ($downA && $downB));

        $semifinal = (($x1 == $x1o) && ($y1 == $y1o))
            || (($x1 == $x2o) && ($y1 == $y2o))
            || (($x2 == $x1o) && ($y2 == $y1o))
            || (($x2 == $x2o) && ($y2 == $y2o));
        return $final && !$semifinal;
    }
}
?>