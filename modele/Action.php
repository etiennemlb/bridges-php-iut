<?php
    require_once('Ville.php');
    require_once('Partie.php');
    require_once('Pont.php');
    
    
    // énumération listant les types d'action pouvant être réalisées par le joueur
    abstract class TypeAction {
        const CreerPont = 0;
        const SupprimerPont = 1;
    }

    // classe représentant une action du joueur 
    // une action crée est supposée comme pouvant être executée 
    // c'est au contrôleur de décider de la création ou non de l'action
    class Action {
        
        // type de l'action
        private $type;

        // les deux villes concernées par l'action
        private $ville1, $ville2;

        // création d'une action de suppression ou ajout de pont entre deux villes
        function __construct($type, VilleNF $ville1, VilleNF $ville2)
        {
            $this->type = $type;
            $this->ville1 = $ville1;
            $this->ville2 = $ville2;
        }

        // méthode qui execute l'action sur une partie
        function executer(Partie $p) {
            switch ($this->type) {
                case TypeAction::CreerPont :
                    $p->creerPont($this->ville1, $this->ville2);
                    break;
                
                case TypeAction::SupprimerPont :
                    $p->supprimerPont($this->ville1, $this->ville2);
                    break;
            }
        }
    }
?>