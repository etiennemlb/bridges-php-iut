<?php

    // script de test des fonctions du modèle

require_once('Partie.php');

$partie = new Partie();

$villes = array();

foreach (range(0, 6) as $i) {
    $villes[intval($partie->villes()->getVille($i)->getId())] = $partie->villes()->getVille($i);
}

$partie->ajouterAction(TypeAction::CreerPont, $villes[4], $villes[5]);
$partie->ajouterAction(TypeAction::CreerPont, $villes[1], $villes[5]);
$partie->ajouterAction(TypeAction::CreerPont, $villes[0], $villes[1]);
$partie->ajouterAction(TypeAction::CreerPont, $villes[0], $villes[2]);
$partie->ajouterAction(TypeAction::CreerPont, $villes[0], $villes[2]);
$partie->ajouterAction(TypeAction::CreerPont, $villes[2], $villes[3]);
$partie->ajouterAction(TypeAction::CreerPont, $villes[2], $villes[3]);
$partie->ajouterAction(TypeAction::CreerPont, $villes[2], $villes[6]);
$partie->ajouterAction(TypeAction::CreerPont, $villes[2], $villes[6]);

$partie->mettreAJour();

$ascii = array();
$cities = array();
$cities[1] = '①';
$cities[2] = '②';
$cities[3] = '③';
$cities[4] = '④';
$cities[5] = '⑤';
$cities[6] = '⑥';
$cities[7] = '⑦';
$cities[8] = '⑧';
$cities[9] = '⑨';
$cities[10] = '⑩';

foreach ($partie->ponts() as $pont) {
    if ($pont->estHorizontal()) {
        $y = $pont->ville1()->getY();
        $xm = min($pont->ville1()->getX(), $pont->ville2()->getX());
        $xM = max($pont->ville1()->getX(), $pont->ville2()->getX());
        foreach (range($xm, $xM) as $x) {
            $ascii[$y][$x] = ($pont->nombrePonts() == 1) ? "―" : "═";
        }
    } else {
        $x = $pont->ville1()->getX();
        $ym = min($pont->ville1()->getY(), $pont->ville2()->getY());
        $yM = max($pont->ville1()->getY(), $pont->ville2()->getY());
        foreach (range($ym, $yM) as $y) {
            $ascii[$y][$x] = ($pont->nombrePonts() == 1) ? "│" : "║";
        }
    }

}

foreach ($partie->villes()->getVilles() as $ville) {
    $x = $ville->getX();
    $y = $ville->getY();
    $ascii[$y][$x] = $ville->getNombrePontsMax() . "" . $ville->getId();
}

?>
<meta charset="utf-8" />
<style>
    span.stretch {
    display:inline-block;
    -webkit-transform:scale(1.4,1); /* Safari and Chrome */
    -moz-transform:scale(1.4,1); /* Firefox */
    -ms-transform:scale(1.4,1); /* IE 9 */
    -o-transform:scale(1.4,1); /* Opera */
    transform:scale(1.4,1); /* W3C */
}

    span.stretchv {
    display:inline-block;
    -webkit-transform:scale(1,1.2); /* Safari and Chrome */
    -moz-transform:scale(1,1.2); /* Firefox */
    -ms-transform:scale(1,1.2); /* IE 9 */
    -o-transform:scale(1,1.2); /* Opera */
    transform:scale(1,1.2); /* W3C */
}

    a {
        text-decoration: none;
        color: #000044;
        text-decoration: blink;
    }

    a.selected {
        text-decoration: none;
        color: #0000ff;
    }

    a:hover {
        text-decoration: none;
        color: #00aa00;
    }
}

</style>
<table cellpadding=0 cellspacing=0 style="font-size: 3em; border: 3px solid black; text-align: center; margin: 0px; border-collapse: collapse; border-spacing: 0">
<?php 
foreach (range(0, $partie->villes()->maxX) as $x) {
    echo "<tr>";
    foreach (range(0, $partie->villes()->maxY) as $y) {
        echo "<td style=\"border: 0.5px solid #eeeeee;\">";
        if (isset($ascii[$x][$y])) {
            if ($ascii[$x][$y] == "═") {
                echo "<span class=\"stretch\">";
            } else if ($ascii[$x][$y] == "│" || $ascii[$x][$y] == "║") {
                echo "<span class=\"stretchv\">";
            } else if ($ascii[$x][$y] > 0) {
                echo "<span>";
                echo "<a href=\"#" . substr($ascii[$x][$y], 1) . "\">";
            }
            echo ($ascii[$x][$y] > 0) ? $cities[intval($ascii[$x][$y][0])] : $ascii[$x][$y];
            if ($ascii[$x][$y] > 0) {
                echo "</a>";
            }
            echo "</span>";

        } else {
            echo "&nbsp;";
        }
        echo "</td>";
    }
    echo "</tr>";
}
?>
</table>