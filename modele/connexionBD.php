<?php
// Classe qui gère les accès à la base de données

// Exception relative à un probleme de connexion
class ConnexionException extends Exception
{
    private $chaine;
    public function __construct($chaine)
    {
        $this->chaine = $chaine;
    }

    public function afficher()
    {
        return $this->chaine;
    }
}

// Classe gérant la connexion à la bd et les requêtes à la bd
class ConnexionBD
{
    private $connexion;

    // Constructeur de la classe
    public function __construct()
    {
        try {
            $chaine = 'mysql:host=' . HOST . ';dbname=' . BD;
            $this->connexion = new PDO($chaine, LOGIN, PASSWORD);
            $this->connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            $exception = new ConnexionException("problème de connexion à la base");
            throw $exception;
        }
    }

    // méthode coupant la connexion à la bd
    public function deconnexion()
    {
        $this->connexion = null;
    }

    // méthode qui vérifie si un coupe pseudo/password est valide et bien présent dans la bd
    public function sessionValide($pseudo, $password)
    {
        $statement = $this->connexion->prepare('select motDePasse from joueurs where pseudo=?;');
        $statement->bindParam(1, $pseudo);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);

        //si pas de pass associé au pseudo
        if (!$result)
            return false;

        return (crypt($password, $result['motDePasse']) == $result['motDePasse']);
    }

    // méthode retournant le couple parties gagnées / parties perdues pour un pseudo
    public function recupererStats($pseudo)
    {
        $statement = $this->connexion->prepare('SELECT partiesJouees,partiesGagnees from joueurs where pseudo=?');
        $statement->bindParam(1, $pseudo);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    // méthode qui incremente le nombre de parties jouées ou gagnées du joueur
    // param :
    // $partieGagnee = true si la partie enregistrée à été gagnée, false sinon
    public function incrementerParties($pseudo, $partieGagnee = false)
    {
        $statement = $this->connexion->prepare('UPDATE `joueurs` SET `partiesjouees`=?,`partiesGagnees`=? WHERE pseudo=?');
        $stats = $this->recupererStats($pseudo);

        $stats['partiesJouees'] += 1;
        $statement->bindParam(1, $stats['partiesJouees']);

        $stats['partiesGagnees'] += ($partieGagnee ? 1 : 0);
        $statement->bindParam(2, $stats['partiesGagnees']);

        $statement->bindParam(3, $pseudo);

        return $statement->execute();
    }


    // méthode retournant la liste des joueurs et leurs statistiques
    public function listeJoueursStats()
    {
        $statement = $this->connexion->prepare('SELECT pseudo,partiesJouees,partiesGagnees from joueurs');
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }
}
?>