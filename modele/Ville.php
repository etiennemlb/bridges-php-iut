<?php

//Classe d'inter compatibilitée avec le modèle fourni
class Ville
{
    public $id;
    public $nombrePontsMax;
    public $nombrePonts;

    function __construct($id, $nombrePontsMax, $nombrePonts)
    {
        $this->id = $id;
        $this->nombrePontsMax = $nombrePontsMax;
        $this->nombrePonts = $nombrePonts;
    }
}

// classe représentant une ville dont les attributs correspondent au modèle utilisé
class VilleNF
{
    // permet d'identifier de manière unique la ville
    private $id;

    // coordonnées de la ville
    private $x, $y;

    // nombre maximum de ponts attachés à la ville
    private $nombrePontsMax;

    // le nombre de ponts connectés à la ville
    private $nombrePonts;

    // constructeur qui permet de valuer les 2 attributs de la classe
    function __construct($id, $x, $y, $nombrePontsMax)
    {
        $this->id = $id;
        $this->x = $x;
        $this->y = $y;
        $this->nombrePontsMax = $nombrePontsMax;
        $this->nombrePonts = 0;
    }

    // sélecteur qui retourne la valeur de l'attribut id
    function getId()
    {
        return $this->id;
    }

    // sélecteur qui retourne la valeur de l'attribut x
    function getX()
    {
        return $this->x;
    }

    // sélecteur qui retourne la valeur de l'attribut y
    function getY()
    {
        return $this->y;
    }
    
    // sélecteur qui retourne la valeur de l'attribut nombrePontsMax
    function getNombrePontsMax()
    {
        return $this->nombrePontsMax;
    }

    // sélecteur qui retourne la valeur de l'attribut nombrePonts
    function getNombrePonts()
    {
        return $this->nombrePonts;
    }

    function setNombrePonts($nb)
    {
        $this->nombrePonts = $nb;
    }

    // méthode qui ajoute un certain nombre de ponts attachées à la ville
    function addNombrePonts($nb)
    {
        $this->nombrePonts += $nb;
    }

    // retourne true si la ville v2 est alignée avec la ville horizontalement ou verticalement
    function aligne(VilleNF $v2)
    {
        return $this->x == $v2->getX()
            or $this->y == $v2->getY();
    }
}
?>
